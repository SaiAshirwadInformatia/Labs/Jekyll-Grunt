# Jekyll Grunt

[![build status](https://gitlab.com/SaiLabs/Jekyll-Grunt/badges/master/build.svg)](https://gitlab.com/SaiLabs/Jekyll-Grunt/commits/master)
[![Gem Version](https://badge.fury.io/rb/jekyll-grunt.svg)](https://badge.fury.io/rb/jekyll-grunt)

This plugin provides support for performing **Grunt** tasks easily

### Installation

This plugin is available as a [RubyGem](https://rubygems.org/gems/jekyll-grunt/)

Add this line to your application's Gemfile:

```
gem 'jekyll-grunt'
```

And then execute the `bundle install` command to install the gem.

Alternatively, you can also manually install the gem using the following command:

```
$ gem install jekyll-grunt
```

After the plugin has been installed successfully, add the following lines to your _config.yml in order to tell Jekyll to use the plugin:

```yaml
gems:
  - jekyll-grunt
```

#### Perform Grunt Task using Jekyll Config

```yaml
grunt:
	- clean
	- uglify
	- stage:prod:copy:production
```

Above line executes your task that you usually perform by calling below

```
grunt clean uglify stage:prod:copy:production
```


### Contribute

Fork this repository, make your changes and then issue a pull request. If you find bugs or have new ideas that you do not want to implement yourself, file a bug report.

### Copyright

Copyright (c) 2016 Sai Ashirwad Informatia

License: MIT