# Generate script code for Google Analytics with Liquid Tag
require 'jekyll'
require 'jekyll/plugin_grunt'
require 'jekyll/plugin_version'

# Register Jekyll Site Post Read hook of Grunt plugin
Jekyll::Hooks.register :site, :after_init do |jekyll| # jekyll here acts as site global object
	JekyllGrunt = Jekyll::Grunt.new()
	JekyllGrunt.resolve(jekyll)
	puts "Reading newly generated Grunt files\n"
	jekyll.reset()
	puts "Jekyll site reset done\n"
	jekyll.setup()
	puts "Jekyll site setup done\n"
	jekyll.read()
	puts "Jekyll site read done\n"
	puts "Looks like everything is ready\n"
end