require 'uri'

module Jekyll
	class Grunt
		def initialize()
			puts "Checking if NPM is available\n"
			fail unless system('which npm')
			puts "Great, NPM is available\n"
			puts "Checking if Grunt is available\n"
			unless system('which grunt')
				puts "Grunt not available\n"
				puts "Installing grunt\n"
				system('npm install -g grunt-cli')
				puts "Installation completed\n"
			else
				puts "Great, Grunt is available\n"
			end
		end

		def resolve(site)
			if File.exists?("Gruntfile.js")
				puts "Performing Grunt Task\n"
				if site.config['grunt']
					puts "Resolving grunt individually configured in config" + "\n"
					site.config['grunt'].each do |task| 
						command = "grunt " + task
						system(command)
					end
				end
			else
				puts "Gruntfile.js does not exists!\n"
			end
		end
	end
end